<?php
/**
 * @file
 * Default template for nano_grade module.
 *
 * @param bool $utf_mode.
 *   Use placeholder markup or UTF characters.
 *
 * @param array $icons
 *   array of icon settings.
 */
?>
<span class="nanograde">
  <?php if ($mode != 'utf'): ?>
    <?php foreach ($icons as $delta => $icon): ?>
      <span class="<?php print $icon['class']; ?>"></span>
    <?php endforeach; ?>
  <?php else: ?>
    <?php foreach ($icons as $delta => $icon): ?>
        <?php print $icon['active'] ? '&#9733;' : '&#9734;'; ?>
    <?php endforeach; ?>
  <?php endif; ?>
</span>
